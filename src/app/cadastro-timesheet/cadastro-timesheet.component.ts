import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-timesheet',
  templateUrl: './cadastro-timesheet.component.html'
})
export class CadastroTimesheetComponent implements OnInit {

  items: string[];

  constructor() {
    this.items = [
      'Paulo',
      'Maria',
      'João',
      'Bruno',
      'Caio',
      'Paula',
      'Robson',
    ];

   }

  ngOnInit() {
  }

}
