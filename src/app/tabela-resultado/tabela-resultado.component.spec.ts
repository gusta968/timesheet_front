import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaResultadoComponent } from './tabela-resultado.component';

describe('TabelaResultadoComponent', () => {
  let component: TabelaResultadoComponent;
  let fixture: ComponentFixture<TabelaResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
