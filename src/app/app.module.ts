import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CadastroTimesheetComponent } from './cadastro-timesheet/cadastro-timesheet.component';
import { BuscaTimesheetComponent } from './busca-timesheet/busca-timesheet.component';
import { CalendarioComponent} from './calendario/calendario.component';
import { LoganEmpresaComponent} from './logan-empresa/logan-empresa.component';
import { ModalComponent} from './modal/modal.component';
import { TabelaResultadoComponent} from './tabela-resultado/tabela-resultado.component';


@NgModule({
  declarations: [
    AppComponent,
    CadastroTimesheetComponent,
    BuscaTimesheetComponent,
    CalendarioComponent,
    LoganEmpresaComponent,
    ModalComponent,
    TabelaResultadoComponent
  
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
