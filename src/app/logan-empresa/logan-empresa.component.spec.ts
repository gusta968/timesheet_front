import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoganEmpresaComponent } from './logan-empresa.component';

describe('LoganEmpresaComponent', () => {
  let component: LoganEmpresaComponent;
  let fixture: ComponentFixture<LoganEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoganEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoganEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
